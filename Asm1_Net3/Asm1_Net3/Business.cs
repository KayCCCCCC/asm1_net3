﻿using System;
using System.Collections.Generic;

namespace Asm1_Net3
{
    public class Business
    {
        public static GiaoVien NhapThongTinGiaoVien()
        {
            Console.Write("Nhap ho ten: ");
            string hoTen = Console.ReadLine();

            Console.Write("Nhap nam sinh: ");
            int namSinh = Convert.ToInt32(Console.ReadLine());

            Console.Write("Nhap luong co ban: ");
            double luongCoBan = Convert.ToDouble(Console.ReadLine());

            Console.Write("Nhap he so luong: ");
            double heSoLuong = Convert.ToDouble(Console.ReadLine());

            // Tạo một đối tượng GiaoVien với thông tin vừa nhập
            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);

            return giaoVien;
        }

        public static void HienThiThongTinLuongThapNhat(List<GiaoVien> danhSachGiaoVien)
        {
            if (danhSachGiaoVien.Count > 0)
            {
                GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];
                foreach (GiaoVien giaoVien in danhSachGiaoVien)
                {
                    if (giaoVien.TinhLuong() < giaoVienLuongThapNhat.TinhLuong())
                    {
                        giaoVienLuongThapNhat = giaoVien;
                    }
                }

                // Hiển thị thông tin của GiaoVien có lương thấp nhất
                Console.WriteLine("\nThong tin GiaoVien co luong thap nhat:");
                giaoVienLuongThapNhat.XuatThongTin();
            }
            else
            {
                Console.WriteLine("\nDanh sach GiaoVien rong");
            }
        }
    }
}
