﻿using Asm1_Net3;
using System;
using System.Collections.Generic;


    public class Program
    {
        static void Main()
        {
            try
            {
                Console.Write("Nhap so luong giao vien: ");
                int soLuong = Convert.ToInt32(Console.ReadLine());

                List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();
                Business business = new Business();

                for (int i = 0; i < soLuong; i++)
                {
                    Console.WriteLine($"\nNhap thong tin cho giao vien thu {i + 1}:");
                    GiaoVien giaoVien = Business.NhapThongTinGiaoVien();
                    danhSachGiaoVien.Add(giaoVien);
                }

                Business.HienThiThongTinLuongThapNhat(danhSachGiaoVien);

                Console.WriteLine("Nhan mot phim bat ky de dung chuong trinh...");
                Console.Read();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }




