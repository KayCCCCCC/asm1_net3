﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asm1_Net3
{

    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien()
        {
            HeSoLuong = 0;
        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
            : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }
        public new void NhapThongTin(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        {
            base.NhapThongTin(hoTen, namSinh, luongCoBan);
            HeSoLuong = heSoLuong;
        }

        public override double TinhLuong()
        {
            return base.TinhLuong() * HeSoLuong * 1.25;
        }

        public override void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}, he so luong: {HeSoLuong}, Luong: {TinhLuong()}");
        }

        public void XuLy()
        {
            HeSoLuong += 0.6;
        }

    }
}
